/* Tests whether constraints in field is passed between method calls. */

class GetterSetter {
	public static void main(String[] args) {
		A a;
		a = new A();
		a.setA(1);
		System.out.println(a.getA());
	}
}

class A {
	int a;

	public void setA(b) {
		a = b;
	}

	public int getA() {
		return a;
	}
}