/*
This program makes sure that the analysis keeps track of which method is actually being called
and the values of the two ints. If the actual method being called (the swap) isn't taken
into account, then the swap (c = a; a = b; b = c;) won't take effect and the resulting interval
will be negative instead.
*/
class SwapObjects {
    public static void main(String[] a){
        System.out.println(new Run().run());
    }
}

class Run {
    public int run(){
        A a;
        A b;
        A c;
        int val1;
        int val2;
        int multiplier;

        // Initialize.
        a = new A();
        b = new B();
        val1 = 0 - 1;
        val2 = 0 - 1;

        // val1 = -1, val2 = -1.
        multiplier = a.get();
        val1 = val1 * multiplier;
        multiplier = b.get();
        val2 = val2 * multiplier;
        // val1 = -1, val2 = +1.

        c = a;
        a = b;
        b = c;

        multiplier = a.get();
        val1 = val1 * multiplier;
        multiplier = b.get();
        val2 = val2 * multiplier;
        // val1 = 1, val2 = 1

        val1 = val1 + val2;
        // val1 = 2.

        return val1;
    }
}

// A.get is multiply by 1.
class A {
    public int get() {
        return 1;
    }
}

// B.get is multiply by -1.
class B extends A {
    public int get() {
        return 0 - 1;
    }
}
