// Test that the analysis should be able to distinguish
// line 9 and 10.
class Test {
    public static void main(String[] x) {
        A a;
        int w;
        w = a.foo(0-3, 4);
        w = a.foo(0-5, 6);
        System.out.println((a.foo(0-3, 4)) + 4);
        System.out.println((a.foo(0-5, 6)) + 6);
    }
}

class A {
    public int foo(int a, int b) {
        System.out.println(a + b);
        return a;
    }
}