// This test case tests the computation of the intervals for x after calling the method m on a and b.

class ComputeMethodInterval {
    public static void main(String[] args) {
        A a;
        A b;
        int x;
        int y;

        a = new A();
        b = new B();
        x = 10;
        y = 25;

        if (y < ((x * 2) + 10)) {
            x = a.m(4);
        } else {
            x = b.m(6);
        }

        System.out.println(x);
    }
}

class A {
    public int m(int in) {
        return ((in * 2) - 10);
    }
}

class B extends A {
    public int m(int in) {
        return ((in * 6) - 20);
    }
}
